package co.za.mpholetsiki.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by me@mpholetsiki.co.za
 */

@Controller
public class HomeController {

	private static final String HOME_VIEW = "home";

	@RequestMapping(value = {"/home"})
	public String login() {
		return HOME_VIEW;
	}

}
