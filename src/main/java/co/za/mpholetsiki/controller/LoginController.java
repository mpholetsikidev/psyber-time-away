package co.za.mpholetsiki.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by me@mpholetsiki.co.za
 */

@Controller
public class LoginController {

	private static final String LOGIN_VIEW = "login";

	@RequestMapping(value = {"/login"})
	public String login() {
		return LOGIN_VIEW;
	}

	@RequestMapping(value = {"/logout"})
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

	@RequestMapping(value = {"/"})
	public String home() {
		return "redirect:/home";
	}
}
