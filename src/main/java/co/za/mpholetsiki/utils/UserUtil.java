package co.za.mpholetsiki.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by me@mpholetsiki.co.za
 */
public class UserUtil {

	public static String getLoggedInUser() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(principal instanceof UserDetails) {
			return ((UserDetails) principal).getUsername();
		} else {
			return principal.toString();
		}
	}

	public static String getLoggedInUserRole() {
		return String.valueOf(SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0]);
	}
}
