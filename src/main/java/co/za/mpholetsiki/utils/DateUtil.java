package co.za.mpholetsiki.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by me@mpholetsiki.co.za
 */
public class DateUtil {

	public static Date convert(String value) {
		if(value != null) {
			DateFormat formatter = new SimpleDateFormat("yyy-MM-dd hh:mm:ss");
			Date date = null;
			try {
				date = formatter.parse(value);
			} catch(ParseException e) {
				e.printStackTrace();
			}
			return date;
		} else {
			return null;
		}
	}

	public static String getDate(Date value) {
		if(value != null) {
			DateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
			return formatter.format(value);
		}
		return null;
	}
}
