package co.za.mpholetsiki.service;

import co.za.mpholetsiki.resource.UserRepository;
import co.za.mpholetsiki.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by me@mpholetsiki.co.za
 */

@Service
public class UserService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public void save(User user) {
		userRepository.save(user);
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return userRepository.findByUsername(email);
	}
}
