package co.za.mpholetsiki.resource;

import co.za.mpholetsiki.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by me@mpholetsiki.co.za
 */
public interface UserRepository extends JpaRepository<User, String> {
	User findByUsername(String username);
}
