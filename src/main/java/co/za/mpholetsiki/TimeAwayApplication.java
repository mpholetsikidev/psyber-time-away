package co.za.mpholetsiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.psybergate")
public class TimeAwayApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimeAwayApplication.class, args);
	}
}
