package co.za.mpholetsiki.constants;

/**
 * Created by me@mpholetsiki.co.za
 */
public enum Role {
	ROLE_ADMIN,
	ROLE_USER;
}
